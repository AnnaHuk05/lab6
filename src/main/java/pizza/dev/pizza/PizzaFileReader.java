package pizza.dev.pizza;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.List;

public class PizzaFileReader {
    public PizzaFileReader() {
    }
    public static List<Pizza> getPizzaList(String path) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(new File(path), objectMapper.getTypeFactory().constructCollectionType(List.class, Pizza.class));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}