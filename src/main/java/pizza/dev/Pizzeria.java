package pizza.dev;

import pizza.dev.dinner.Dinner;
import pizza.dev.dinner.DinnerFileReader;
import pizza.dev.pizza.Pizza;
import pizza.dev.pizza.PizzaFileReader;

import java.util.List;

public class Pizzeria {
    private final List<Dinner> dinnerList;
    private final List<Pizza> menu;

    public Pizzeria(String dinnerPath, String menuPath) {
        menu = PizzaFileReader.getPizzaList(menuPath);
        dinnerList = DinnerFileReader.getDinnerList(dinnerPath, menu);
    }

    public List<Dinner> getDinnerList() {
        return dinnerList;
    }

    public List<Pizza> getMenu() {
        return menu;
    }
}
