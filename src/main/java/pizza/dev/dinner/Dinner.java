package pizza.dev.dinner;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import pizza.dev.pizza.Pizza;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
@JsonSerialize

public class Dinner {
    private String name;
    private List<Pizza> order;
    private  String address;
    private LocalDateTime deliveryDateTime;

    @JsonCreator
    public Dinner(@JsonProperty("name") String name,
                  @JsonProperty("order") List<Pizza> order,
                  @JsonProperty("address") String address,
                  @JsonProperty("deliveryDateTime") LocalDateTime deliveryDateTime) {
        if(order == null || order.isEmpty()) {
            throw new IllegalArgumentException("Order cannot be null or empty");
        }
        this.name = name;
        this.order = order;
        this.address = address;
        this.deliveryDateTime = deliveryDateTime;
    }
    public Dinner() {
        if(order == null || order.isEmpty()) {
            throw new IllegalArgumentException("Order cannot be null or empty");
        }
    }
    public int getPrice() {
        return order.stream().mapToInt(Pizza::getPrice).sum();
    }

    public int getWeight() {
        return order.stream().mapToInt(Pizza::getWeight).sum();
    }

    public boolean isSpicy() {
        return order.stream().anyMatch(Pizza::isSpicy);
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<Pizza> getOrder() {
        return order;
    }

    public LocalDateTime getDeliveryDateTime() {
        return deliveryDateTime;
    }
    public void setOrder(List<Pizza> pizzas)
    {
        order=pizzas;
    }
    @Override
    public String toString() {
        String orderString = order.stream().map(Pizza::getName).reduce((a, b) -> a + ", " + b).orElse("");
        return String.format("%-10s | %-40s | %-5d UAH | %-20s | %s", name, orderString, getPrice(), address,
                deliveryDateTime.format(DateTimeFormatter.ofPattern("HH:mm dd.MM.yyyy"))
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dinner other) {
            return name.equals(other.name) && order.equals(other.order) &&
                    address.equals(other.address) && deliveryDateTime.equals(other.deliveryDateTime);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + order.hashCode() + address.hashCode() + deliveryDateTime.hashCode();
    }
}
