package pizza.dev.pizza;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.List;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonSerialize
public class Pizza {
    private final String name;
    private final int weight;
    private final int price;
    private final List<String> toppings;
    private final boolean isSpicy;

    @JsonCreator
    public Pizza(@JsonProperty("name") String name,
                 @JsonProperty("weight") int weight,
                 @JsonProperty("price") int price,
                 @JsonProperty("toppings") List<String> toppings,
                 @JsonProperty("isSpicy") boolean isSpicy)  {
        if (weight < 0) {
            throw new IllegalArgumentException("Weight cannot be negative");
        }
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }
        if (toppings == null || toppings.isEmpty()) {
            throw new IllegalArgumentException("Toppings cannot be null or empty");
        }

        this.name = name;
        this.weight = weight;
        this.price = price;
        this.toppings = List.copyOf(toppings);
        this.isSpicy = isSpicy;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getWeight() {
        return weight;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public boolean isSpicy() {
        return isSpicy;
    }

    @Override
    public String toString() {
        String toppingsString = String.join(", ", toppings);
        String spiciness = isSpicy ? "spicy" : "not spicy";
        return String.format("%-20s | %-5d g | %-5d UAH | %-30s | %s",
                name, weight, price, toppingsString, spiciness);
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pizza other) {
            return name.equals(other.name) && weight == other.weight &&
                    price == other.price && toppings.equals(other.toppings) && isSpicy == other.isSpicy;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + weight + price + toppings.hashCode() + (isSpicy ? 1 : 0);
    }
}
