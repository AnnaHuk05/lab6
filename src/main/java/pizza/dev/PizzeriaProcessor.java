package pizza.dev;

import pizza.dev.dinner.Dinner;
import pizza.dev.pizza.Pizza;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PizzeriaProcessor {
    private final Pizzeria pizzeria;

    public PizzeriaProcessor(String dinnerPath, String menuPath) {
        pizzeria = new Pizzeria(dinnerPath, menuPath);
    }

    public List<Pizza> getMenu() {
        return pizzeria.getMenu();
    }

    public List<Dinner> getDinnerList() {
        return pizzeria.getDinnerList();
    }

    public List<Dinner> getSortedByTimeDinnerList() {
        return pizzeria.getDinnerList().stream().sorted(Comparator.comparing(Dinner::getDeliveryDateTime)).toList();
    }

    public List<Dinner> getSortedByPriceDinnerList() {
        return pizzeria.getDinnerList().stream().sorted(Comparator.comparing(Dinner::getPrice)).toList();
    }

    public List<Dinner> getDinnersByPizzaName(String pizzaName) {
        return pizzeria.getDinnerList().stream().filter
                (dinner -> dinner.getOrder().stream().anyMatch(pizza -> pizza.getName().equals(pizzaName))).toList();
    }

    public List<Dinner> getDinnersHavingMorePizzasThat(int num){
        return pizzeria.getDinnerList().stream().filter(dinner -> dinner.getOrder().size() > num).toList();
    }

    public Dinner getDinnerWithBiggestOrder() {
        return pizzeria.getDinnerList().stream().max(Comparator.comparing(Dinner::getPrice)).get();
    }

    public Map<Pizza,List<Dinner>> getGroupByPizzaMap() {
        return pizzeria.getDinnerList().stream().flatMap(dinner -> dinner.getOrder().stream().
                map(pizza -> Map.entry(pizza, dinner))).collect(
                        Collectors.groupingBy(Map.Entry::getKey,
                                Collectors.mapping(Map.Entry::getValue,
                                        Collectors.toList())));
    }

    public Map<Dinner, Duration> getExpiresDinnersMap(){
        return pizzeria.getDinnerList().stream().filter(dinner ->
                dinner.getDeliveryDateTime().isBefore(LocalDateTime.now())).collect(Collectors.toMap(
                dinner -> dinner,
                dinner -> Duration.between(dinner.getDeliveryDateTime(), LocalDateTime.now())));
    }

    public List<Dinner> getDinnersWithSpicyPizza(){
        return pizzeria.getDinnerList().stream().filter(Dinner::isSpicy).toList();
    }

    public void serializeCollection(){
        PizzeriaSerializer.serializePizzaCollection(pizzeria.getMenu(),"C:\\java-labs-lab6\\src\\main\\resources\\backUp.json");
    }
}
