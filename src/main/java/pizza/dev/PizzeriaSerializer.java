package pizza.dev;

import pizza.dev.pizza.Pizza;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class PizzeriaSerializer {
            public static void serializePizzaCollection(List<Pizza> pizzaList, String filePath) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

            try {
                objectMapper.writeValue(new File(filePath), pizzaList);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
